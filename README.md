Docker GO Development
====

This project is a base project for Golang development in Docker with automatic reloading.

**TL;DR***: clone the project, run `make run` and go to http://$(boot2docker ip):8080/


I've gathered a lot of different things from around the web on how to write Go in Docker. The
requirements I was trying to solve were as follows.

 - should have the tiniest container possible
 - should be able to rebuild on code changes


## Tiny Container

When I say **tiny** I mean really tiny.  I would say one of the top 3 reasons I love Go is that my
code can compile to a single *binary* file with no external dependencies. Having said that, the
common way to run Go in Docker is to run it with a
[Go docker][] container. This seems contrary to the
Golang fundamentals. I don't need any Go binaries on my executing machines. In order to accomplish
this We need to compile Go before it gets to the machine.

When we run `go build` we compile our Go executable for the *active* machine. I write code on a
Macbook Pro and I run it on linux (docker) so I cannot run `go build` in OSX and expect the binary
to run in linux. But Go can cross-compile for another operating system and here's how.

```
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .
```

**NOTE**: The "app" at the end is the name of the executable. It can be changed, but requires a few more
changes in the rest of this codebase. 

The code above will compile a single binary for linux without CGO enabled. This allows our binary
file to be executed on *any* linux machine. Even one without native C libraries. Credit for this
goes to [@ngauthier][]. Now that we have this binary file we can use the [scratch][] docker image
which is exactly 0B in size. Tiny: accomplished.

## Auto Building

Before we can automate, we need to scrunch the build command. I used Make for this. The Make task
will build our "app" file when any specified files have been modified. Pair that with [entr(1)][]
and you get rebuilding. 

```
ag -l . | entr -cdr make run
```

NOTE:  I use `ag` because it's fast and it only lists files that are deemed relevant. See more
[here][ag]

Run this in terminal and your code will run when any relevant files are modified.


 [Go docker]: https://registry.hub.docker.com/_/golang/
 [scratch]: https://docs.docker.com/articles/baseimages/#creating-a-simple-base-image-using-scratch
 [@ngauthier]: https://blog.codeship.com/building-minimal-docker-containers-for-go-applications/
 [entr(1)]: http://entrproject.org/
 [ag]: https://github.com/ggreer/the_silver_searcher
