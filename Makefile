

# rwildcard
# 
# To find all the C files in the current directory:
# $(call rwildcard, , *.c)
#
# To find all the .c and .h files in src:
# $(call rwildcard, src/, *.c *.h)
rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

watchfiles=$(call rwildcard, , *.go *.tmpl)

app: $(watchfiles)
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .
	docker-compose build

.PHONY: run
run: app
	docker-compose up
