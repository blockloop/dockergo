package main

import (
	"net/http"
)

func main() {
	http.ListenAndServe(":8080", http.HandlerFunc(serve))
}

func serve(res http.ResponseWriter, req *http.Request) {
	res.Write([]byte("Hello world!"))
}
